#!/bin/bash
curl v4.ifconfig.co > ip.txt
awk '{ print $0 "/32" }' < ip.txt > ipnew.txt
export stuff=$(cat ipnew.txt)
aws ec2 authorize-security-group-ingress --group-name launch-wizard-8  --protocol tcp --port 22 --cidr $stuff
